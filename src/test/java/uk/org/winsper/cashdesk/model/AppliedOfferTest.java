package uk.org.winsper.cashdesk.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import uk.org.winsper.cashdesk.model.AppliedOffer;
import uk.org.winsper.cashdesk.service.offer.OfferImplementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
public class AppliedOfferTest {
	@Mock
	OfferImplementation offer;
	
	@Test
	public void testAppliedOfferText() {
		given(offer.getOfferText()).willReturn("Kittens 50% off");
		
		AppliedOffer appliedOffer = new AppliedOffer(offer, 2, -5);
		assertEquals("Kittens 50% off: -5p", appliedOffer.getAppliedOfferText());
	}
}
