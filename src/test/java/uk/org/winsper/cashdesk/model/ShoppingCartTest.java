package uk.org.winsper.cashdesk.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import uk.org.winsper.cashdesk.entity.GroceryItem;

import static uk.org.winsper.cashdesk.ExampleGroceryItems.*;

/**
 * Tests the Shopping Cart. Note that the creation of ShoppingCart instances
 * is already tested in ShoppingServiceTest, as the two are heavily linked.
 * @author darren
 *
 */
public class ShoppingCartTest {
	@Test
	public void testGetItemNotInCart() {
		List<GroceryItem> items = new ArrayList<>();
		ShoppingCart cart = new ShoppingCart(items);
		
		assertNull(cart.getItemByName("Does not exist"));
	}
	
	@Test
	public void testGetItemInCart() {
		String itemName = "TestItem";
		GroceryItem item = new GroceryItem(itemName, 0L);
		
		List<GroceryItem> items = new ArrayList<>();
		items.add(item);
		ShoppingCart cart = new ShoppingCart(items);
		
		assertEquals(item, cart.getItemByName(itemName).getItem());
	}
	
	@Test
	public void testSubtotal() {
		// 3 pears and 2 sausages comes out to 30 + 40 = 70
		ShoppingCart cart = new ShoppingCart(Arrays.asList(new GroceryItem[] {pear, pear, pear, sausages, sausages}));
		assertEquals(70, cart.getSubtotal());
	}
}
