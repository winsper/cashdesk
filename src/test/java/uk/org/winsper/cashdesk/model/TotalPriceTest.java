package uk.org.winsper.cashdesk.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import uk.org.winsper.cashdesk.model.AppliedOffer;
import uk.org.winsper.cashdesk.model.TotalPrice;

@ExtendWith(MockitoExtension.class)
class TotalPriceTest {
	@Mock
	AppliedOffer appliedOffer;
	
	@Test
	void testTotalPriceWithNoOffers() {
		TotalPrice total = new TotalPrice(200, 50, new ArrayList<>());
		
		String expectedOutcome = "Subtotal: " + getNewNumberFormatter().format(2.00) + System.lineSeparator()
			+ "(no offers available)" + System.lineSeparator()
			+ "Total: 50p";
		
		assertEquals(expectedOutcome, total.getTextualRepresentation());
	}
	
	@Test
	void testTotalPriceWithOffers() {
		String appliedOfferText = "Puppies 25% off: -20p";
		
		given(appliedOffer.getAppliedOfferText()).willReturn(appliedOfferText);
		TotalPrice total = new TotalPrice(90, 150, Collections.singletonList(appliedOffer));
		
		String expectedOutcome = "Subtotal: 90p" + System.lineSeparator()
			+ appliedOfferText + System.lineSeparator()
			+ "Total: " + getNewNumberFormatter().format(1.50);
		
		assertEquals(expectedOutcome, total.getTextualRepresentation());
	}

	private NumberFormat getNewNumberFormatter() { return NumberFormat.getCurrencyInstance(); }
}
