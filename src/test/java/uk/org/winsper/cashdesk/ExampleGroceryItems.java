package uk.org.winsper.cashdesk;

import uk.org.winsper.cashdesk.entity.GroceryItem;

public class ExampleGroceryItems {
	private static final String DISCOUNTED_ITEM = "Pear";
	private static final long DISCOUNTED_ITEM_PRICE = 10;
	public static final GroceryItem pear = new GroceryItem(DISCOUNTED_ITEM, DISCOUNTED_ITEM_PRICE);
	
	private static final String NOT_DISCOUNTED_ITEM = "Sausages";
	private static final long NOT_DISCOUNTED_ITEM_PRICE = 20;
	public static final GroceryItem sausages = new GroceryItem(NOT_DISCOUNTED_ITEM, NOT_DISCOUNTED_ITEM_PRICE);
}
