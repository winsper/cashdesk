package uk.org.winsper.cashdesk;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * A dummy application for testing
 * @author darren
 *
 */
@SpringBootApplication
public class TestApplication {
	// Do nothing
}
