package uk.org.winsper.cashdesk.service.offer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import uk.org.winsper.cashdesk.entity.GroceryItem;
import uk.org.winsper.cashdesk.entity.Offer;
import uk.org.winsper.cashdesk.model.AppliedOffer;
import uk.org.winsper.cashdesk.model.ShoppingCart;

import static uk.org.winsper.cashdesk.ExampleGroceryItems.*;

/**
 * Tests the Discount class
 * @author darren
 */
public class DiscountTest {
	/**
	 * Tests that the correct discount is applied to
	 * the correct items only.
	 */
	@Test
	public void testDiscountedPears() {
		ShoppingCart cart = buildExampleCart();
		Offer discountOffer = getDiscountedPearsOffer();

		discountOffer.setOfferItem(pear);

		OfferImplementation discount = new Discount(discountOffer);
		AppliedOffer appliedOffer = discount.applyOffer(cart);
		
		assertEquals("Pears 20% off", discount.getOfferText());
		// There should be a 6p discount (2p/20% for each pear)
		assertEquals(-6, appliedOffer.getBalanceModifier());
		assertEquals(discount, appliedOffer.getOffer());
		// The discount should be applied 3 times
		assertEquals(3, appliedOffer.getAppliedCount());
	}
	
	@Test
	public void testDiscountNotApplicableToCart() {
		ShoppingCart cart = buildSausagesOnlyCart();
		Offer discountOffer = getDiscountedPearsOffer();

		discountOffer.setOfferItem(pear);

		OfferImplementation discount = new Discount(discountOffer);
		AppliedOffer appliedOffer = discount.applyOffer(cart);
		
		assertNull(appliedOffer);
	}
	
	private ShoppingCart buildExampleCart() {
		List<GroceryItem> items = new ArrayList<>();
		items.add(pear);
		items.add(pear);
		items.add(pear);
		items.add(sausages);
		items.add(sausages);
		
		return new ShoppingCart(items);
	}
	
	private ShoppingCart buildSausagesOnlyCart() {
		List<GroceryItem> items = new ArrayList<>();
		items.add(sausages);
		items.add(sausages);
		
		return new ShoppingCart(items);
	}
	
	private Offer getDiscountedPearsOffer() {
		Offer offer = new Offer();
		offer.setOfferItem(pear);
		offer.setPercentChange(-20);
		offer.setOfferType(OfferType.DISCOUNT.toString());
		
		return offer;
	}
}
