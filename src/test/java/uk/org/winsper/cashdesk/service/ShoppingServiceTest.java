package uk.org.winsper.cashdesk.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import uk.org.winsper.cashdesk.entity.GroceryItem;
import uk.org.winsper.cashdesk.entity.Offer;
import uk.org.winsper.cashdesk.exception.InvalidGroceryItemsException;
import uk.org.winsper.cashdesk.exception.UnknownOfferTypeException;
import uk.org.winsper.cashdesk.model.AppliedOffer;
import uk.org.winsper.cashdesk.model.ShoppingCart;
import uk.org.winsper.cashdesk.model.TotalPrice;
import uk.org.winsper.cashdesk.repo.GroceryItemRepository;
import uk.org.winsper.cashdesk.service.offer.OfferImplementation;

import static org.mockito.BDDMockito.*;
import static uk.org.winsper.cashdesk.ExampleGroceryItems.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ShoppingServiceTest {
	@MockBean
	GroceryItemRepository itemRepository;
	
	@MockBean
	OfferService offerService;
	
	@Mock
	OfferImplementation offer;
	
	@Autowired
	ShoppingService service;
	
	private List<String> validNames = Arrays.asList(new String[] {"Cat", "Dog", "Mouse", "Snake"});
	
	@BeforeEach
	private void classSetup() {
		given(itemRepository.findByNameIgnoreCase(anyString())).will(new Answer<GroceryItem>() {
			public GroceryItem answer(InvocationOnMock invocation) {
				String name = invocation.getArgument(0);
				return getItem(name);
			}
		});
	}
	
	/**
	 * Test that invalid items are detected and that the "correct" invalid item is found.
	 */
	@Test
	public void testInvalidItems() {
		List<String> values = Arrays.asList(new String[] { "Cat", "Dog", "Giraffe", "Snake" });
		
		InvalidGroceryItemsException e = assertThrows(InvalidGroceryItemsException.class, () -> service.createShoppingCart(values));
		assertEquals(1, e.getInvalidItems().size());
		assertEquals("Giraffe", e.getInvalidItems().get(0));
	}
	
	/**
	 * Tests that a set of valid items is parsed correctly.
	 * @throws InvalidGroceryItemsException
	 */
	@Test
	public void testValidItems() throws InvalidGroceryItemsException {
		List<String> values = Arrays.asList(new String[] { "Cat", "Cat", "Dog", "Snake", "Dog", "Dog" });
		
		ShoppingCart cart = service.createShoppingCart(values);
		assertEquals(3, cart.getItems().size());
		assertEquals("Cat", cart.getItems().get(0).getItem().getName());
		assertEquals(2, cart.getItems().get(0).getCount());
		assertEquals("Dog", cart.getItems().get(1).getItem().getName());
		assertEquals(3, cart.getItems().get(1).getCount());
		assertEquals("Snake", cart.getItems().get(2).getItem().getName());
		assertEquals(1, cart.getItems().get(2).getCount());
	}
	
	@Test
	public void testTotalPrice() throws UnknownOfferTypeException {
		ShoppingCart cart = new ShoppingCart(Arrays.asList(new GroceryItem[] { sausages, sausages, pear, pear }));
		Offer offerEntity = new Offer();
		
		given(offerService.getAllOffers()).willReturn(getOfferList());
		
		// An applied offer that fakes 10% off sausages
		AppliedOffer appliedOffer = new AppliedOffer(offer, 2, -4);
		given(offer.applyOffer(cart)).willReturn(appliedOffer);
		
		TotalPrice total = service.calculateTotalPrice(cart);
		assertEquals(60, total.getSubTotal(), "The subtotal is incorrect");
		assertEquals(56, total.getTotal(), "The total is incorrect, implies offers are not applied correctly");
		assertEquals(1, total.getAppliedOffers().size(), "The list of applied offers is incorrect");
		assertEquals(appliedOffer, total.getAppliedOffers().get(0), "The wrong offer was applied");
	}
	
	private GroceryItem getItem(String name) {
		return validNames.contains(name) ? new GroceryItem(name, 0L) : null;
	}
	
	private List<OfferImplementation> getOfferList() {
		List<OfferImplementation> offers = new ArrayList<>();
		offers.add(offer);
		return offers;
	}
}
