package uk.org.winsper.cashdesk.service.offer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import uk.org.winsper.cashdesk.entity.GroceryItem;
import uk.org.winsper.cashdesk.entity.Offer;
import uk.org.winsper.cashdesk.model.AppliedOffer;
import uk.org.winsper.cashdesk.model.ShoppingCart;

import static uk.org.winsper.cashdesk.ExampleGroceryItems.*;

/**
 * Tests the Discount class
 * @author darren
 */
public class LinkedDiscountTest {
	/**
	 * Tests that the correct discount is applied to
	 * the correct items only.
	 */
	@Test
	public void testDiscountedPears() {
		ShoppingCart cart = buildExampleCart();
		Offer discountOffer = getDiscountedPearsOffer();

		OfferImplementation discount = new LinkedDiscount(discountOffer);
		AppliedOffer appliedOffer = discount.applyOffer(cart);
		
		assertEquals("Buy 2 Sausages, get 1 Pear 20% off", discount.getOfferText());
		// There should be a 2p discount (2p/20% for 1 pear as there's 2 sausages)
		assertEquals(-2, appliedOffer.getBalanceModifier());
		assertEquals(discount, appliedOffer.getOffer());
		// The discount should be applied 1 time
		assertEquals(1, appliedOffer.getAppliedCount());
	}
	
	@Test
	public void testMultipleDiscountedPears() {
		ShoppingCart cart = buildExampleCartExtraSausages();
		Offer discountOffer = getDiscountedPearsOffer();

		OfferImplementation discount = new LinkedDiscount(discountOffer);
		AppliedOffer appliedOffer = discount.applyOffer(cart);
		
		assertEquals("Buy 2 Sausages, get 1 Pear 20% off", discount.getOfferText());
		// There should be a 4p discount (4p/20% for 2 pears as there's 4 sausages)
		assertEquals(-4, appliedOffer.getBalanceModifier());
		assertEquals(discount, appliedOffer.getOffer());
		// The discount should be applied 2 times
		assertEquals(2, appliedOffer.getAppliedCount());
	}
	
	@Test
	public void testDiscountNotApplicableToCart() {
		ShoppingCart cart = buildSausagesOnlyCart();
		Offer discountOffer = getDiscountedPearsOffer();

		OfferImplementation discount = new LinkedDiscount(discountOffer);
		AppliedOffer appliedOffer = discount.applyOffer(cart);
		
		assertNull(appliedOffer);
	}
	
	private ShoppingCart buildExampleCart() {
		List<GroceryItem> items = new ArrayList<>();
		items.add(pear);
		items.add(pear);
		items.add(pear);
		items.add(sausages);
		items.add(sausages);
		
		return new ShoppingCart(items);
	}
	
	private ShoppingCart buildExampleCartExtraSausages() {
		List<GroceryItem> items = new ArrayList<>();
		items.add(pear);
		items.add(pear);
		items.add(pear);
		items.add(sausages);
		items.add(sausages);
		items.add(sausages);
		items.add(sausages);
		
		return new ShoppingCart(items);
	}
	
	private ShoppingCart buildSausagesOnlyCart() {
		List<GroceryItem> items = new ArrayList<>();
		items.add(sausages);
		items.add(sausages);
		
		return new ShoppingCart(items);
	}
	
	private Offer getDiscountedPearsOffer() {
		Offer offer = new Offer();
		offer.setOfferItem(pear);
		offer.setPercentChange(-20);
		offer.setRequiredItem(sausages);
		offer.setDiscountedItemCount(1);
		offer.setRequiredPurchaseCount(2);
		offer.setOfferType(OfferType.LINKED_DISCOUNT.toString());
		
		return offer;
	}
}
