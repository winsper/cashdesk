package uk.org.winsper.cashdesk;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import uk.org.winsper.cashdesk.CurrencyUtil;

import java.text.NumberFormat;

class CurrencyUtilTest {
	@Test
	void testPennies() {
		assertEquals("-50p", CurrencyUtil.getCurrencyValue(-50));
	}
	
	@Test
	void testPounds() {
	    assertEquals(getNewNumberFormatter().format(1.23), CurrencyUtil.getCurrencyValue(123));
	}

	private NumberFormat getNewNumberFormatter() { return NumberFormat.getCurrencyInstance(); }
}
