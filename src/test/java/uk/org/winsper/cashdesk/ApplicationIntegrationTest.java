package uk.org.winsper.cashdesk;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import uk.org.winsper.cashdesk.exception.InvalidGroceryItemsException;
import uk.org.winsper.cashdesk.exception.UnknownOfferTypeException;
import uk.org.winsper.cashdesk.model.ShoppingCart;
import uk.org.winsper.cashdesk.model.TotalPrice;
import uk.org.winsper.cashdesk.service.ShoppingService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ApplicationIntegrationTest {

	@Autowired
	private ShoppingService service;
	
	@Test
	void testSingleItem() throws InvalidGroceryItemsException, UnknownOfferTypeException {
		ShoppingCart cart = service.createShoppingCart(Collections.singletonList("Cat"));
		TotalPrice price = service.calculateTotalPrice(cart);
		
		String expectedPrice = "Subtotal: 10p" + System.lineSeparator()
				+ "(no offers available)" + System.lineSeparator()
				+ "Total: 10p";
		assertEquals(expectedPrice, price.getTextualRepresentation());
	}
	
	@Test
	void testMultipleItemsNoOffer() throws InvalidGroceryItemsException, UnknownOfferTypeException {
		ShoppingCart cart = service.createShoppingCart(Arrays.asList("Cat", "Cat", "Cat", "Dog", "Dog", "Dog", "Dog"));
		TotalPrice price = service.calculateTotalPrice(cart);
		NumberFormat format = getNewNumberFormatter();
		
		String expectedPrice = "Subtotal: "+ format.format(1.10) + System.lineSeparator()
				+ "(no offers available)" + System.lineSeparator()
				+ "Total: "+ format.format(1.10);
		assertEquals(expectedPrice, price.getTextualRepresentation());
	}
	
	@Test
	void testMultipleOfSameOffer() throws InvalidGroceryItemsException, UnknownOfferTypeException {
		ShoppingCart cart = service.createShoppingCart(Arrays.asList("Snake", "Snake", "Snake"));
		TotalPrice price = service.calculateTotalPrice(cart);
		NumberFormat format = getNewNumberFormatter();

		String expectedPrice = "Subtotal: "+ format.format(1.20) + System.lineSeparator()
				+ "Snakes 10% off: -12p" + System.lineSeparator()
				+ "Total: "+ format.format(1.08);
		assertEquals(expectedPrice, price.getTextualRepresentation());
	}
	
	@Test
	void testLinkedOffer() throws InvalidGroceryItemsException, UnknownOfferTypeException {
		ShoppingCart cart = service.createShoppingCart(Arrays.asList("Cat", "Cat", "Mouse"));
		TotalPrice price = service.calculateTotalPrice(cart);
		
		String expectedPrice = "Subtotal: 50p" + System.lineSeparator()
				+ "Buy 2 Cats, get 1 Mouse 50% off: -15p" + System.lineSeparator()
				+ "Total: 35p";
		assertEquals(expectedPrice, price.getTextualRepresentation());
	}
	
	@Test
	void testDifferentOffers() throws InvalidGroceryItemsException, UnknownOfferTypeException {
		ShoppingCart cart = service.createShoppingCart(Arrays.asList("Cat", "Cat", "Mouse", "Snake", "Snake"));
		TotalPrice price = service.calculateTotalPrice(cart);
		NumberFormat format = getNewNumberFormatter();
		
		String priceText = price.getTextualRepresentation();
		
		// The order the offers are applied is non-deterministic, so need to use a different
		// way of testing the expected text.
		assertNotEquals(-1, priceText.indexOf("Subtotal: "+ format.format(1.30)));
		assertNotEquals(-1, priceText.indexOf("Buy 2 Cats, get 1 Mouse 50% off: -15p"));
		assertNotEquals(-1, priceText.indexOf("Snakes 10% off: -8p"));
		assertNotEquals(-1, priceText.indexOf("Total: "+ format.format(1.07)));
	}

	private NumberFormat getNewNumberFormatter() { return NumberFormat.getCurrencyInstance(); }
}
