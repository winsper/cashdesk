package uk.org.winsper.cashdesk.repo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import uk.org.winsper.cashdesk.repo.GroceryItemRepository;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class GroceryItemRepositoryTest {
	@Autowired
	GroceryItemRepository repository;
	
	@ParameterizedTest
	@ValueSource(strings = { "CAT", "dog", "Mouse", "Snake" })
	void testDataPresent(String id) {
	    assertNotNull(repository.findByNameIgnoreCase(id));
	}
}
