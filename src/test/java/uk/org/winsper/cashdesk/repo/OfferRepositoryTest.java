package uk.org.winsper.cashdesk.repo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import uk.org.winsper.cashdesk.repo.OfferRepository;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class OfferRepositoryTest {
	@Autowired
	OfferRepository repository;
	
	@Test
	void testDataPresent() {
	    assertEquals(repository.findAll().size(), 2);
	}
}
