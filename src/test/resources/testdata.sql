insert into groceryitem (name, price)
values
	('Cat', 10),
	('Dog', 20),
	('Mouse', 30),
	('Snake', 40);

insert into offer 
	(offer_type,
	percent_change,
	required_purchase_count,
	required_item_name,
	discounted_item_count,
	offer_item_name)
values
	('LINKED_DISCOUNT', -50, 2, 'Cat', 1, 'Mouse'),
	('DISCOUNT', -10, 0, null, 0, 'Snake');