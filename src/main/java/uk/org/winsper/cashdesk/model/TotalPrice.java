package uk.org.winsper.cashdesk.model;

import java.util.List;

import uk.org.winsper.cashdesk.CurrencyUtil;

public class TotalPrice {
	private long subTotal;
	private long total;
	private List<AppliedOffer> appliedOffers;
	
	public TotalPrice(long subTotal, long total, List<AppliedOffer> appliedOffers) {
		super();
		this.subTotal = subTotal;
		this.total = total;
		this.appliedOffers = appliedOffers;
	}

	public long getSubTotal() {
		return subTotal;
	}

	public long getTotal() {
		return total;
	}

	public List<AppliedOffer> getAppliedOffers() {
		return appliedOffers;
	}
	
	public String getTextualRepresentation() {
		StringBuilder sb = new StringBuilder();
		sb.append("Subtotal: " + CurrencyUtil.getCurrencyValue(subTotal));
		sb.append(System.lineSeparator());
		if (appliedOffers != null && appliedOffers.size() > 0) {
			appliedOffers.forEach(offer -> sb.append(offer.getAppliedOfferText() + System.lineSeparator()));
		} else {
			sb.append("(no offers available)");
			sb.append(System.lineSeparator());
		}
		sb.append("Total: " + CurrencyUtil.getCurrencyValue(total));
		
		return sb.toString();
	}
}
