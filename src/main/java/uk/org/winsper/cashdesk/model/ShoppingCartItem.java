package uk.org.winsper.cashdesk.model;

import uk.org.winsper.cashdesk.entity.GroceryItem;

public class ShoppingCartItem {
	private GroceryItem item;
	private int count;
	
	public ShoppingCartItem(GroceryItem item, int count) {
		super();
		this.item = item;
		this.count = count;
	}

	public int getCount() {
		return count;
	}

	public void incrementCount() {
		this.count++;
	}

	public GroceryItem getItem() {
		return item;
	}
}
