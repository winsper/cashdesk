package uk.org.winsper.cashdesk.model;

import uk.org.winsper.cashdesk.service.offer.OfferImplementation;

/**
 * Represents an offer applied to a shopping cart
 * @author darren
 *
 */
public class AppliedOffer {
	private OfferImplementation offer;
	private int appliedCount;
	private long balanceModifier;

	public AppliedOffer(OfferImplementation offer, int appliedCount, long balanceModifier) {
		super();
		this.offer = offer;
		this.appliedCount = appliedCount;
		this.balanceModifier = balanceModifier;
	}
	
	public OfferImplementation getOffer() {
		return offer;
	}

	/**
	 * Gets the number of times the offer has been applied to the cart
	 * @return
	 */
	public int getAppliedCount() {
		return appliedCount;
	}

	/**
	 * Gets the total change to the price of the shopping cart
	 * (i.e. discount per item * number of items)
	 * @return The amount to change the balance by in pence
	 */
	public long getBalanceModifier() {
		return balanceModifier;
	}
	
	/**
	 * Generates a textual representation of the applied offer
	 * @return the textual representation of the applied offer
	 */
	public String getAppliedOfferText() {
		return String.format("%s: %dp", getOffer().getOfferText(), getBalanceModifier());
	}
}
