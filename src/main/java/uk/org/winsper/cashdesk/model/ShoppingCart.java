package uk.org.winsper.cashdesk.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.org.winsper.cashdesk.entity.GroceryItem;

/**
 * Represents a cart of grocery items. The cart is immutable once created as there is
 * no need for interactivity in this application, but it could be extended to had add()
 * and remove() methods if needed.
 * @author darren
 */
public class ShoppingCart {
	private List<ShoppingCartItem> items = new ArrayList<>();
	private Map<String, ShoppingCartItem> itemMap = new HashMap<>();
	
	/**
	 * Creates a shopping cart based on the given list of grocery items.
	 * A list is expected because customers generally expect their shopping carts
	 * to have a predictable order to the items in the cart.
	 * @param items
	 */
	public ShoppingCart(List<GroceryItem> items) {
		items.stream().forEach(this::addItem);
	}
	
	/***
	 * Gets an unmodifiable list of items in the cart.
	 * @return a List of items in the cart. Note that this list is not modifiable.
	 */
	public List<ShoppingCartItem> getItems() {
		return Collections.unmodifiableList(items);
	}
	
	/**
	 * Returns a shopping cart item with the specified name if it exists.
	 * @param name
	 * @return a ShoppingCartItem where getItem().getName() equals the specified name.
	 *         If no such item is in the cart, null is returned.
	 */
	public ShoppingCartItem getItemByName(String name) {
		if (itemMap.containsKey(name)) {
			return itemMap.get(name);
		} else {
			return null;
		}
	}
	
	/**
	 * Gets the current price of the shopping cart with no offers applied
	 * @return The current subtotal in pence
	 */
	public long getSubtotal() {
		return items.stream().mapToLong(item -> item.getCount() * item.getItem().getPrice()).sum();
	}
	
	private void addItem(GroceryItem item) {
		if (!itemMap.containsKey(item.getName())) {
			ShoppingCartItem cartItem = new ShoppingCartItem(item, 1);
			itemMap.put(item.getName(), cartItem);
			items.add(cartItem);
		} else {
			itemMap.get(item.getName()).incrementCount();
		}
	}
}
