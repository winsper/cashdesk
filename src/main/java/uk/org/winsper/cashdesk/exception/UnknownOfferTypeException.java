package uk.org.winsper.cashdesk.exception;

public class UnknownOfferTypeException extends Exception {
	private static final long serialVersionUID = -6630892507404028583L;
	private static final String UNKNOWN_OFFER_TYPE_MESSAGE = "Unknown offer type: %s";
	
	public UnknownOfferTypeException(String offerType) {
		super(String.format(UNKNOWN_OFFER_TYPE_MESSAGE, offerType));
	}
}
