/**
 * 
 */
package uk.org.winsper.cashdesk.exception;

import java.util.List;

/**
 * @author darren
 * Thrown when one or more invalid grocery items are detected.
 */
public class InvalidGroceryItemsException extends Exception {
	private static final long serialVersionUID = -2197130628564193123L;

	private static final String INVALID_MESSAGE = "Some invalid grocery items were detected: %s";
	
	private List<String> invalidItems;
	
	public InvalidGroceryItemsException(List<String> invalidItems) {
		super(String.format(INVALID_MESSAGE, String.join(", ", invalidItems)));
		this.invalidItems = invalidItems;
	}

	public List<String> getInvalidItems() {
		return invalidItems;
	}

	public void setInvalidItems(List<String> invalidItems) {
		this.invalidItems = invalidItems;
	}
}
