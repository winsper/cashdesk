package uk.org.winsper.cashdesk.repo;

import java.util.List;

import org.springframework.data.repository.Repository;

import uk.org.winsper.cashdesk.entity.Offer;

@org.springframework.stereotype.Repository
public interface OfferRepository extends Repository<Offer, Long> {
	public List<Offer> findAll();
}
