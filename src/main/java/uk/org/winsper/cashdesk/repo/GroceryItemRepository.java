package uk.org.winsper.cashdesk.repo;

import org.springframework.data.repository.Repository;

import uk.org.winsper.cashdesk.entity.GroceryItem;

@org.springframework.stereotype.Repository
public interface GroceryItemRepository extends Repository<GroceryItem, String> {
	GroceryItem findByNameIgnoreCase(String name);
}
