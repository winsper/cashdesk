package uk.org.winsper.cashdesk;

import java.math.BigDecimal;
import java.text.NumberFormat;

public class CurrencyUtil {
	public static String getCurrencyValue(long priceInPence) {
		if (priceInPence < 100) {
			return String.format("%dp", priceInPence);
		} else {
			NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
			BigDecimal bdPrice = new BigDecimal(priceInPence).divide(new BigDecimal(100));
			return currencyFormatter.format(bdPrice);
		}
	}
}
