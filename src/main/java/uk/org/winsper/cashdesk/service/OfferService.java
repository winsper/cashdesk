package uk.org.winsper.cashdesk.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import uk.org.winsper.cashdesk.entity.Offer;
import uk.org.winsper.cashdesk.exception.UnknownOfferTypeException;
import uk.org.winsper.cashdesk.repo.OfferRepository;
import uk.org.winsper.cashdesk.service.offer.Discount;
import uk.org.winsper.cashdesk.service.offer.LinkedDiscount;
import uk.org.winsper.cashdesk.service.offer.OfferImplementation;
import uk.org.winsper.cashdesk.service.offer.OfferType;

@Service
public class OfferService {
	@Autowired
	private OfferRepository offerRepository;
	
	public List<OfferImplementation> getAllOffers() throws UnknownOfferTypeException {
		List<OfferImplementation> offers = new ArrayList<>();
		
		for (Offer offer : offerRepository.findAll()) {
			offers.add(getOfferImplementation(offer));
		}
		
		return offers;
	}
	
	private OfferImplementation getOfferImplementation(Offer offer) throws UnknownOfferTypeException {
		switch (OfferType.forName(offer.getOfferType())) {
		case LINKED_DISCOUNT:
			return new LinkedDiscount(offer);
		case DISCOUNT:
			return new Discount(offer);
		default:
			throw new UnknownOfferTypeException(offer.getOfferType());
		}
	}
}
