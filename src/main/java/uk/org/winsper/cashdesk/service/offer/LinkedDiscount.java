package uk.org.winsper.cashdesk.service.offer;

import java.math.BigDecimal;

import uk.org.winsper.cashdesk.entity.Offer;
import uk.org.winsper.cashdesk.model.AppliedOffer;
import uk.org.winsper.cashdesk.model.ShoppingCart;
import uk.org.winsper.cashdesk.model.ShoppingCartItem;

public class LinkedDiscount implements OfferImplementation {
	
	private Offer offerDetails;
	
	public LinkedDiscount(Offer offerDetails) {
		super();
		this.offerDetails = offerDetails;
	}

	@Override
	public AppliedOffer applyOffer(ShoppingCart cart) {
		ShoppingCartItem requiredItem = cart.getItemByName(offerDetails.getRequiredItem().getName());
		ShoppingCartItem discountedItem = cart.getItemByName(offerDetails.getOfferItem().getName());
		AppliedOffer appliedOffer = null;
		
		if (discountedItem != null && requiredItem != null) {
			// Determine the whole number of times the offer can be applied
			int applicableCount = requiredItem.getCount() / offerDetails.getRequiredPurchaseCount();
			// The number of items that can be discounted
			int candidateItemCount = discountedItem.getCount() / offerDetails.getDiscountedItemCount();
			
			// The number of times the offer can actually be applied is the minimum of the two above
			int appliedCount = Integer.min(applicableCount, candidateItemCount);
			
			BigDecimal percentage = new BigDecimal(offerDetails.getPercentChange());
			BigDecimal multiplicand = percentage.divide(new BigDecimal(100));
			BigDecimal subTotal = new BigDecimal(discountedItem.getItem().getPrice() * appliedCount);
			
			if (discountedItem != null) {
				appliedOffer = new AppliedOffer(
						this,
						appliedCount,
						subTotal.multiply(multiplicand).longValue());
			}
		}
		
		return appliedOffer;
	}

	@Override
	public String getOfferText() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(String.format("Buy %d %s, get %d %s %d%% off",
				offerDetails.getRequiredPurchaseCount(),
				getPlural(offerDetails.getRequiredItem().getName(), offerDetails.getRequiredPurchaseCount()),
				offerDetails.getDiscountedItemCount(),
				getPlural(offerDetails.getOfferItem().getName(), offerDetails.getDiscountedItemCount()),
				offerDetails.getPercentChange() * -1));
		
		return sb.toString();
	}

	private static String getPlural(String noun, int count) {
		if (count == 1) {
			return noun;
		}
		return noun.endsWith("s") ? noun : noun + "s";
	}
}
