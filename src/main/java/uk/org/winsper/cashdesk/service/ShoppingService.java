package uk.org.winsper.cashdesk.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import uk.org.winsper.cashdesk.entity.GroceryItem;
import uk.org.winsper.cashdesk.exception.InvalidGroceryItemsException;
import uk.org.winsper.cashdesk.exception.UnknownOfferTypeException;
import uk.org.winsper.cashdesk.model.AppliedOffer;
import uk.org.winsper.cashdesk.model.ShoppingCart;
import uk.org.winsper.cashdesk.model.TotalPrice;
import uk.org.winsper.cashdesk.repo.GroceryItemRepository;
import uk.org.winsper.cashdesk.service.offer.OfferImplementation;

@Service
public class ShoppingService {
	@Autowired
	private GroceryItemRepository itemRepository;
	
	@Autowired
	private OfferService offerService;
	
	/**
	 * Parses the list of grocery items into GroceryItem objects and then adds them to a shopping cart.
	 * @param items A list of items to add to the shopping cart
	 * @return a constructed shopping cart
	 * @throws InvalidGroceryItemsException if any of the items are invalid (not in the database)
	 */
	public ShoppingCart createShoppingCart(List<String> items) throws InvalidGroceryItemsException {
		return new ShoppingCart(parseGroceryItems(items));
	}
	
	public TotalPrice calculateTotalPrice(ShoppingCart cart) throws UnknownOfferTypeException {
		long subTotal = cart.getSubtotal();
		List<OfferImplementation> availableOffers = offerService.getAllOffers();
		List<AppliedOffer> appliedOffers = availableOffers
				.stream()
				.map(offer -> offer.applyOffer(cart))
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		long total = subTotal + appliedOffers.stream().mapToLong(offer -> offer.getBalanceModifier()).sum();
		return new TotalPrice(subTotal, total, appliedOffers);
	}
	
	/**
	 * Parses a list of strings into a list of GroceryItem instances.
	 * @param items
	 * @return a list of parsed GroceryItem instances.
	 * @throws InvalidGroceryItemsException if any of the strings are not in the database.
	 */
	private List<GroceryItem> parseGroceryItems(List<String> items) throws InvalidGroceryItemsException {
		List<GroceryItem> parsedItems = new ArrayList<>();
		List<String> invalidItems = new ArrayList<>();
		
		for (String item : items) {
			GroceryItem parsedItem = itemRepository.findByNameIgnoreCase(item);
			if (parsedItem == null) {
				invalidItems.add(item);
			} else {
				parsedItems.add(parsedItem);
			}
		}
		
		if (invalidItems.size() > 0) {
			throw new InvalidGroceryItemsException(invalidItems);
		}
		
		return parsedItems;
	}
}
