package uk.org.winsper.cashdesk.service.offer;

import uk.org.winsper.cashdesk.model.AppliedOffer;
import uk.org.winsper.cashdesk.model.ShoppingCart;

/**
 * An interface that represents the implementation of a specifc offer type
 * @author darren
 *
 */
public interface OfferImplementation {
	/**
	 * Applies the offer
	 * @param cart The shopping cart to apply the offer to
	 * @return an AppliedOffer object that denotes the discount and number of times the discount was applied
	 */
	public AppliedOffer applyOffer(ShoppingCart cart);

	/**
	 * Generates a textual description of the offer
	 * @return a String describing the offer
	 */
	String getOfferText();
}
