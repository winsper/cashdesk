package uk.org.winsper.cashdesk.service.offer;

import java.math.BigDecimal;

import uk.org.winsper.cashdesk.entity.Offer;
import uk.org.winsper.cashdesk.model.AppliedOffer;
import uk.org.winsper.cashdesk.model.ShoppingCart;
import uk.org.winsper.cashdesk.model.ShoppingCartItem;

/**
 * Implements a basic discount to the specified item
 * @author darren
 *
 */
public class Discount implements OfferImplementation {

	private Offer offerDetails;
	
	public Discount(Offer offerDetails) {
		super();
		this.offerDetails = offerDetails;
	}

	@Override
	public AppliedOffer applyOffer(ShoppingCart cart) {
		ShoppingCartItem cartItem = cart.getItemByName(offerDetails.getOfferItem().getName());
		AppliedOffer appliedOffer = null;
		
		if (cartItem != null) {
			BigDecimal percentage = new BigDecimal(offerDetails.getPercentChange());
			BigDecimal multiplicand = percentage.divide(new BigDecimal(100));
			BigDecimal subTotal = new BigDecimal(cartItem.getItem().getPrice() * cartItem.getCount());
			
			if (cartItem != null) {
				appliedOffer = new AppliedOffer(
						this,
						cartItem.getCount(),
						subTotal.multiply(multiplicand).longValue());
			}
		}
		
		return appliedOffer;
	}
	
	@Override
	public String getOfferText() {
		return String.format("%s %d%% off", getPlural(offerDetails.getOfferItem().getName()), offerDetails.getPercentChange() * -1);
	}

	private static String getPlural(String noun) {
		return noun.endsWith("s") ? noun : noun + "s";
	}
	
}
