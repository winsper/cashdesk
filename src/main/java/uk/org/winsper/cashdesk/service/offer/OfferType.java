package uk.org.winsper.cashdesk.service.offer;

public enum OfferType {
	LINKED_DISCOUNT("LINKED_DISCOUNT"),
	DISCOUNT("DISCOUNT");
	
	private final String value;
	
	OfferType(String offerType) {
		this.value = offerType;
	}
	
	@Override
	public String toString() {
		return value;
	}
	
	private static final OfferType[] copyOfValues = values();
	
	public static OfferType forName(String name) {
        for (OfferType value : copyOfValues) {
            if (value.name().equals(name)) {
                return value;
            }
        }
        return null;
    }
}
