package uk.org.winsper.cashdesk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "groceryitem")
public class GroceryItem {
	@Id
	@Column
	private String name;
	
	@Column
	private Long price;

	protected GroceryItem () {}

	public GroceryItem(String name, Long price) {
		super();
		this.name = name;
		this.price = price;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}
}
