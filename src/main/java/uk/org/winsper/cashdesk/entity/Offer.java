package uk.org.winsper.cashdesk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="offer")
public class Offer {
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String offerType;
	
	@Column
	private int percentChange;
	
	@Column
	private int requiredPurchaseCount;
	
	@Column
	private int discountedItemCount;

	@ManyToOne(optional=true)
	private GroceryItem requiredItem;
	
	@ManyToOne(optional=false)
	private GroceryItem offerItem;
	
	public Offer() {}
	
	public Offer(String offerType, int percentChange, int requiredPurchaseCount, int discountedItemCount,
			GroceryItem requiredItem, GroceryItem offerItem) {
		super();
		this.offerType = offerType;
		this.percentChange = percentChange;
		this.requiredPurchaseCount = requiredPurchaseCount;
		this.discountedItemCount = discountedItemCount;
		this.requiredItem = requiredItem;
		this.offerItem = offerItem;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public int getPercentChange() {
		return percentChange;
	}

	public void setPercentChange(int percentChange) {
		this.percentChange = percentChange;
	}

	public int getRequiredPurchaseCount() {
		return requiredPurchaseCount;
	}

	public void setRequiredPurchaseCount(int requiredPurchaseCount) {
		this.requiredPurchaseCount = requiredPurchaseCount;
	}

	public int getDiscountedItemCount() {
		return discountedItemCount;
	}

	public void setDiscountedItemCount(int discountedItemCount) {
		this.discountedItemCount = discountedItemCount;
	}

	public GroceryItem getRequiredItem() {
		return requiredItem;
	}

	public void setRequiredItem(GroceryItem requiredItem) {
		this.requiredItem = requiredItem;
	}

	public GroceryItem getOfferItem() {
		return offerItem;
	}

	public void setOfferItem(GroceryItem offerItem) {
		this.offerItem = offerItem;
	}
}
