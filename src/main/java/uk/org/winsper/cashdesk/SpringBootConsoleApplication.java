package uk.org.winsper.cashdesk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

import uk.org.winsper.cashdesk.exception.InvalidGroceryItemsException;
import uk.org.winsper.cashdesk.exception.UnknownOfferTypeException;
import uk.org.winsper.cashdesk.model.ShoppingCart;
import uk.org.winsper.cashdesk.model.TotalPrice;
import uk.org.winsper.cashdesk.service.ShoppingService;

import static java.lang.System.exit;

import java.lang.invoke.MethodHandles;
import java.util.Arrays;

@SpringBootApplication
@Profile({ "production" })
public class SpringBootConsoleApplication implements CommandLineRunner {
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Autowired
	ShoppingService shoppingService;
	
    public static void main(String[] args) throws Exception {
    	//disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(SpringBootConsoleApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
        
        exit(0);
    }

    @Override
    public void run(String... args) {
        if (args.length > 0 ) {
        	try {
				ShoppingCart cart = shoppingService.createShoppingCart(Arrays.asList(args));
				TotalPrice price = shoppingService.calculateTotalPrice(cart);
				System.out.println(price.getTextualRepresentation());
			} catch (InvalidGroceryItemsException e) {
				logger.error("Failure parsing arguments", e);
			} catch (UnknownOfferTypeException e) {
				logger.error("Error calculating price total", e);
			}
        }else{
            System.out.println("No groceries supplied");
        }
    }
}