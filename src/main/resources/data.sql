insert into groceryitem (name, price)
values
	('Soup', 65),
	('Bread', 80),
	('Milk', 130),
	('Apples', 100);

insert into offer 
	(offer_type,
	percent_change,
	required_purchase_count,
	required_item_name,
	discounted_item_count,
	offer_item_name)
values
	('LINKED_DISCOUNT', -50, 2, 'Soup', 1, 'Bread'),
	('DISCOUNT', -10, 0, null, 0, 'Apples');