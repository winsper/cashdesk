# CashDesk

This is an implementation of a simple till system posed as a coding challenge.

## Building and Running

To build, use the standard `mvn clean install` method.

To run, you can use `java -jar target/cashdesk-1.0.jar` or the supplied `PriceBasket` script.

## Business Logic Decisions

The following decisions were made during development to fill in ambiguities in the task:

- The same offer can be applied more than once, but it must be applied to different items
- Offers are not mutually exclusive, each offer is applied in a vacuum
    - If you make offers interact, you end up with order of precedence problems
    
## Architecture

The application itself follows a standard Spring Boot Application/Service/Repository/Entity model,
with the following packages (uk.org.winsper.cashback base):

- (top level): Holds the application and utility classes (there's only one right now)
- Entity: Holds database entities
- Exception: Custom exceptions the application uses
- Model: Model objects passed around the application
- Repo: Spring Data repositories (save a lot of time writing hibernate queries!)
- Service: Services exposed by the application
- Offer: The implementation of offers

The general flow of the application is as follows:

1. Application launch (SpringBootConsoleApplication)
1. Arguments parsed and shopping cart created (ShoppingService, creates a list of GroceryItems which are then put into a ShoppingCart)
1. Prices calculated (ShoppingService)
    1. ShoppingService calls OfferService to apply offers
    1. OfferService iterates through all offers, applying the offers that are applicable
    1. A TotalPrice is generated from the cart and applied offers
 1. The total price is outputted on the console
 
 ## Database Usage
 
 The system uses an H2 database to store the set of possible grocery items and offers. This is to allow easy additions and changes
 to both grocery items and offers without having to issue new builds.
 
 ## Tests
 
 The tests are built in JUnit 5 as it's starting to mature and most dependencies now support it. The tests are mostly
 unit tests, but there is a set of integraton tests in ApplicationIntegrationTest.